//
//  BuildItTests.swift
//  BuildItTests
//
//  Created by Lee Maguire on 07/03/2017.
//  Copyright © 2017 Lee Maguire. All rights reserved.
//

import XCTest
@testable import BuildIt

class BuildItTests: XCTestCase {
    
    fileprivate let api = APIService()
    fileprivate let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
    
    fileprivate final var staticResponse: WeatherResponse {
        get {
            var dict = [String:AnyHashable]()
            let path = Bundle.main.path(forResource: "weather", ofType: "json")
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path!))
                dict = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String:AnyHashable]
            } catch {
                XCTAssertNotNil(dict)
            }
            
            return WeatherResponse.init(dict)
        }
    }
    
    func test_APIService_returnsResponse() {
        let ex = expectation(description: "weather api GET")

        api.retrieveFiveDayForcast("Dublin", "IE", .metric) { (response, error) in
            if let _ = response {
                ex.fulfill()
            }
            
            XCTAssertNil(error, "API error \(error?.description)")
        }
        
        waitForExpectations(timeout: 5) { error in
            XCTAssertNil(error, "API timeout")
        }
    }
    
    func test_weatherResponse_jsonInit() {

        let response = staticResponse
        XCTAssertTrue(response.forcasts.count == 40, "Did not parse correctly, results not 40")
        
        let firstForcast = response.currentForcast()!
        XCTAssertTrue(firstForcast.date!.timeIntervalSince1970 == 1489093200.0, "Did not parse correctly, date incorrect")
        XCTAssertTrue(firstForcast.weather!.description! == "overcast clouds", "Did not parse correctly, wrong description")
        XCTAssertTrue(firstForcast.weather!.sky! == .cloudy, "Did not parse correctly, wrong sky type")

        XCTAssertTrue(firstForcast.main!.minTemp == 7.8499999999999996, "Did not parse correctly, wrong temp")
        XCTAssertTrue(firstForcast.main!.maxTemp == 8.0899999999999999, "Did not parse correctly, wrong temp")
        XCTAssertTrue(firstForcast.main!.currentTemp == 8.0899999999999999, "Did not parse correctly, wrong temp")

    }
    
}
